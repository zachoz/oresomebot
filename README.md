OresomeBot | IRC Channel administration made easier.
==========

OresomeBot is an IRC bot designed for primary use in OresomeCraft IRC channels as well as a few others.

OresomeBot uses the PircBotX library for it's IRC functions. (https://code.google.com/p/pircbotx/)

Majority of OresomeBot's functions are operator functions. These functions are intented to be used by voiced users so that they're still able to place bans, kick users, etc. It also provides an easier way to execute certain commands (.mute, !kickban, etc).


Keep in mind that OresomeBot is a private project and is coded specifcally for our use. We do a lot of our functions different to how most would purely for our own convenience and to keep the code as simple as possible.

Features:
==========
* Allow for "channel moderators" (voiced users).
* Mod commands like .ban, .kick, !kickban, .mute, etc.
* Channel Management commands. (Requires ChanServ flag +F or custom setup)
* Leave messages for users.
* Inbuilt cleverbot functions, yes, you can speak to it!
* Easy and flexible configuration.

To do:
==========
* Add link grabber
* Add youtube video info grabber
